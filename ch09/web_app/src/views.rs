use actix_web::web::ServiceConfig;

mod auth;
mod to_do;
mod users;

pub fn views_factory(app: &mut ServiceConfig) {
    auth::auth_views_factory(app);
    to_do::to_to_views_factory(app);
    users::user_views_factory(app);
}
