use crate::database::DB;
use crate::json_serialization::login::Login;
use crate::json_serialization::login_response::LoginResponse;
use crate::jwt::JwtToken;
use crate::models::users::user::User;
use crate::schema::users;
use actix_web::{web, HttpResponse};
use diesel::prelude::*;

pub async fn login(credentials: web::Json<Login>, db: DB) -> HttpResponse {
    let mut db_connection = db.connection;
    let users = users::table
        .filter(users::columns::username.eq(credentials.username.clone()))
        .load::<User>(&mut db_connection)
        .unwrap();
    if users.is_empty() {
        return HttpResponse::NotFound().await.unwrap();
    } else if users.len() > 1 {
        return HttpResponse::Conflict().await.unwrap();
    }
    match users[0].clone().verify(credentials.password.clone()) {
        true => {
            let user_id = users[0].clone().id;
            let token = JwtToken::new(user_id);
            let raw_token = token.encode();
            let response = LoginResponse {
                token: raw_token.clone(),
            };
            HttpResponse::Ok()
                .append_header(("token", raw_token))
                .json(&response)
        }
        false => HttpResponse::Unauthorized().finish(),
    }
}
