use crate::database::DB;
use crate::json_serialization::new_user::NewUserSchema;
use crate::models::users::new_user::NewUser;
use crate::schema::users;
use actix_web::{web, HttpResponse, Responder};
use diesel::prelude::*;

pub async fn create(new_user: web::Json<NewUserSchema>, db: DB) -> impl Responder {
    let mut db_connection = db.connection;
    let new_user = NewUser::new(
        new_user.name.clone(),
        new_user.email.clone(),
        new_user.password.clone(),
    );
    let insert_result = diesel::insert_into(users::table)
        .values(&new_user)
        .execute(&mut db_connection);

    match insert_result {
        Ok(_) => HttpResponse::Created(),
        Err(_) => HttpResponse::Conflict(),
    }
}
