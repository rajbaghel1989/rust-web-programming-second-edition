import '../App.css';

const ToDoItem = ({ title, status, sendRequest }) => (
  <div className='itemContainer'>
    <p>{title}</p>
    <div
      className='actionButton'
      onClick={() => sendRequest(title, status)}
    >{getButtonLabel(status)}</div>
  </div>
);

const getButtonLabel = status => {
  return status === 'PENDING' ?
    'edit' :
    'delete';
};

export {
  ToDoItem,
};
