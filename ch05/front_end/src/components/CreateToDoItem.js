import { useState } from 'react';
import axios from 'axios';

const CreateToDoItem = ({ setUpdated }) => {
  const [ title, setTitle ] = useState('');

  const handleTitleChange = event => {
    if (event?.target?.value) {
      setTitle(event.target.value);
    }
  }

  const createItem = event => {
    event.preventDefault();
    if (title) {
      axios.post('http://127.0.0.1:8080/v1/item/create/' + title, {}, {})
      .then(() => {
        setTitle('');
        setUpdated(true);
      });
    }
  };

  return <div className='inputContainer'>
    <input
      type='text'
      id='name'
      placeholder='create to do item'
      value={title}
      onChange={handleTitleChange}
    />
    <div
      className='actionButton'
      id='createButton'
      onClick={createItem}
    >Create
    </div>
  </div>;
};

export {
  CreateToDoItem,
};
