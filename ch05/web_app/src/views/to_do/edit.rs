use crate::json_serialization::to_do_item::ToDoItem;
use crate::json_serialization::to_do_items::ToDoItems;
use crate::jwt::JwtToken;
use crate::processes::process_input;
use crate::state::load_state;
use crate::to_do::enums::TaskStatus;
use crate::to_do::to_do_factory;
use actix_web::{web, HttpResponse};

pub async fn edit(to_do_item: web::Json<ToDoItem>, token: JwtToken) -> HttpResponse {
    println!("here is the message in the token: {}", token.message);
    let state = load_state();
    let status = match &state.get(&to_do_item.title) {
        Some(result) => TaskStatus::from_string(result.as_str().unwrap()),
        None => {
            return HttpResponse::NotFound().json(format!("{} not in state", &to_do_item.title));
        }
    };
    let existing_item = to_do_factory(to_do_item.title.as_str(), status.clone());
    if status == TaskStatus::from_string(&to_do_item.status) {
        return HttpResponse::Ok().json(ToDoItems::get_state());
    }
    process_input(existing_item, "edit".to_owned(), &state);
    HttpResponse::Ok().json(ToDoItems::get_state())
}
