use crate::json_serialization::to_do_item::ToDoItem;
use crate::json_serialization::to_do_items::ToDoItems;
use crate::jwt::JwtToken;
use crate::processes::process_input;
use crate::state::load_state;
use crate::to_do::enums::TaskStatus;
use crate::to_do::to_do_factory;
use actix_web::{web, HttpResponse};

pub async fn delete(to_do_item: web::Json<ToDoItem>, _token: JwtToken) -> HttpResponse {
    let state = load_state();
    let status = match state.get(&to_do_item.title) {
        Some(result) => TaskStatus::from_string(result.as_str().unwrap()),
        None => {
            return HttpResponse::NotFound().json(format!("{} not in state", to_do_item.title));
        }
    };
    let existing_item = to_do_factory(&to_do_item.title, status);
    process_input(existing_item, "delete".to_owned(), &state);
    HttpResponse::Ok().json(ToDoItems::get_state())
}
