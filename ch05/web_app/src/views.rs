use actix_web::web::ServiceConfig;

mod app;
mod auth;
mod to_do;

pub fn views_factory(app: &mut ServiceConfig) {
    auth::auth_views_factory(app);
    to_do::to_to_views_factory(app);
    app::app_views_factory(app);
}
