use crate::state::save_state;
use serde_json::{json, Map, Value};

pub trait Create {
    fn create(&self, title: &String, status: &String, state: &mut Map<String, Value>) {
        state.insert(title.to_string(), json!(status));
        save_state(state);
        println!("\n\n{} is being created\n\n", title);
    }
}
