use actix_web::web::{self, post, scope, ServiceConfig};

mod create;
mod edit;
mod get;

pub fn to_to_views_factory(app: &mut ServiceConfig) {
    app.service(
        scope("v1/item")
            .route("get", web::get().to(get::get_all))
            .route("create/{title}", post().to(create::create))
            .route("edit", post().to(edit::edit)),
    );
}
