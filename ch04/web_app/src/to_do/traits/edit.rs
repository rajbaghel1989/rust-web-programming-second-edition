use crate::state::save_state;
use crate::to_do::enums::TaskStatus;
use serde_json::{json, Map, Value};

pub trait Edit {
    fn set_to_done(&self, title: &String, state: &mut Map<String, Value>) {
        state.insert(title.to_string(), json!(TaskStatus::Done.stringify()));
        save_state(state);
        println!("\n\n{} is being set to done\n\n", title);
    }
    fn set_to_pending(&self, title: &String, state: &mut Map<String, Value>) {
        state.insert(title.to_string(), json!(TaskStatus::Pending.stringify()));
        save_state(state);
        println!("\n\n{} is being set to pending\n\n", title);
    }
}
