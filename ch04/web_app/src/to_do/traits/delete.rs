use crate::state::save_state;
use serde_json::{Map, Value};

pub trait Delete {
    fn delete(&self, title: &String, state: &mut Map<String, Value>) {
        state.remove(title);
        save_state(state);
        println!("\n\n{} is being deleted\n\n", title);
    }
}
