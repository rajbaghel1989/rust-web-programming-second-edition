use crate::config::Config;
use actix_web::dev::Payload;
use actix_web::error::ErrorUnauthorized;
use actix_web::{Error, FromRequest, HttpRequest};
use chrono::Utc;
use futures::future::{err, ok, Ready};
use jsonwebtoken::{decode, encode, DecodingKey, EncodingKey, Header, Validation};
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct JwtToken {
    pub user_id: i32,
    pub exp: usize,
}

impl JwtToken {
    pub fn new(user_id: i32) -> Self {
        let config = Config::new();
        let minutes = config
            .map
            .get("EXPIRE_MINUTES")
            .unwrap_or(&serde_yaml::Value::default())
            .as_i64()
            .unwrap_or(60);
        let expiration = Utc::now()
            .checked_add_signed(chrono::Duration::minutes(minutes))
            .expect("invalid timestamp")
            .timestamp();
        JwtToken {
            user_id,
            exp: expiration as usize,
        }
    }

    pub fn from_token(token: String) -> Result<Self, String> {
        let key = DecodingKey::from_secret(JwtToken::get_key().as_ref());
        let token_result = decode::<JwtToken>(&token, &key, &Validation::default());
        match token_result {
            Ok(data) => Ok(data.claims),
            Err(error) => Err(format!("{}", error)),
        }
    }

    fn get_key() -> String {
        let config = Config::new();
        let key_str = config.map.get("SECRET_KEY").unwrap().as_str().unwrap();
        key_str.to_owned()
    }

    pub fn encode(self) -> String {
        let key = EncodingKey::from_secret(JwtToken::get_key().as_ref());
        encode(&Header::default(), &self, &key).unwrap()
    }
}

impl FromRequest for JwtToken {
    type Error = Error;
    type Future = Ready<Result<JwtToken, Error>>;

    fn from_request(req: &HttpRequest, _: &mut Payload) -> Self::Future {
        match req.headers().get("token") {
            Some(data) => {
                let raw_token = data.to_str().unwrap().to_string();
                let token_result = JwtToken::from_token(raw_token);
                match token_result {
                    Ok(token) => ok(token),
                    Err(message) => {
                        if message == "ExpiredSignature" {
                            err(ErrorUnauthorized("token expired"))
                        } else {
                            err(ErrorUnauthorized("token can't be decoded"))
                        }
                    }
                }
            }
            None => err(ErrorUnauthorized("token not in header under key 'token'")),
        }
    }
}
