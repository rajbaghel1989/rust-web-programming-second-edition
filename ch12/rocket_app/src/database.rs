use crate::config::Config;
use diesel::r2d2::{ConnectionManager, Pool, PooledConnection};
use diesel::PgConnection;
use lazy_static::lazy_static;

type PgPool = Pool<ConnectionManager<PgConnection>>;
pub struct DbConnection {
    pub db_connection: PgPool,
}

lazy_static! {
    pub static ref DBCONNECTION: DbConnection = {
        let config = Config::new();
        let connection_string = config.map.get("DATABASE_URL").unwrap().as_str().unwrap();
        DbConnection {
            db_connection: PgPool::builder()
                .max_size(8)
                .build(ConnectionManager::new(connection_string))
                .expect("failed to create db connection_pool"),
        }
    };
}

pub struct DB {
    pub connection: PooledConnection<ConnectionManager<PgConnection>>,
}
