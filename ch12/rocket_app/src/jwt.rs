use crate::config::Config;
use chrono::Utc;
use jsonwebtoken::{decode, encode, DecodingKey, EncodingKey, Header, Validation};
use rocket::http::Status;
use rocket::request::{FromRequest, Outcome, Request};
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub enum JwTokenError {
    Missing,
    Invalid,
    Expired,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct JwtToken {
    pub user_id: i32,
    pub exp: usize,
}

impl JwtToken {
    pub fn new(user_id: i32) -> Self {
        let config = Config::new();
        let minutes = config
            .map
            .get("EXPIRE_MINUTES")
            .unwrap_or(&serde_yaml::Value::default())
            .as_i64()
            .unwrap_or(60);
        let expiration = Utc::now()
            .checked_add_signed(chrono::Duration::minutes(minutes))
            .expect("invalid timestamp")
            .timestamp();
        JwtToken {
            user_id,
            exp: expiration as usize,
        }
    }

    pub fn from_token(token: String) -> Result<Self, String> {
        let key = DecodingKey::from_secret(JwtToken::get_key().as_ref());
        let token_result = decode::<JwtToken>(&token, &key, &Validation::default());
        match token_result {
            Ok(data) => Ok(data.claims),
            Err(error) => Err(format!("{}", error)),
        }
    }

    fn get_key() -> String {
        let config = Config::new();
        let key_str = config.map.get("SECRET_KEY").unwrap().as_str().unwrap();
        key_str.to_owned()
    }

    pub fn encode(self) -> String {
        let key = EncodingKey::from_secret(JwtToken::get_key().as_ref());
        encode(&Header::default(), &self, &key).unwrap()
    }
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for JwtToken {
    type Error = JwTokenError;

    async fn from_request(req: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        match req.headers().get_one("token") {
            Some(data) => {
                let raw_token = data.to_string();
                let token_result = JwtToken::from_token(raw_token);
                match token_result {
                    Ok(token) => Outcome::Success(token),
                    Err(message) => {
                        if message == "ExpiredSignature" {
                            Outcome::Failure((Status::BadRequest, JwTokenError::Expired))
                        } else {
                            Outcome::Failure((Status::BadRequest, JwTokenError::Invalid))
                        }
                    }
                }
            }
            None => Outcome::Failure((Status::BadRequest, JwTokenError::Missing)),
        }
    }
}
