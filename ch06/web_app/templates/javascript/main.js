function renderItems(items, processType, elementId, processFunction) {
    let itemsMeta = [];
    let placeholder = '<div>';
    items.forEach(({ title: title }) => {
        const placeholderId = `${processType}-${title.replace(' ', '-')}`;
        placeholder += `<div class="itemContainer"><p>${title}</p> <div class="actionButton" id="${placeholderId}">${processType}</div></div>`;
        itemsMeta.push({'id': placeholderId, 'title': title});
    });
    placeholder += '</div>';
    document.getElementById(elementId).innerHTML = placeholder;

    itemsMeta.forEach(({ id: id }) => {
        document.getElementById(id).addEventListener('click', processFunction);
    });
}

function renderHeader(done_item_count, pending_item_count) {
    document.getElementById('completeNum').innerHTML = done_item_count;
    document.getElementById('pendingNum').innerHTML = pending_item_count;
}

function apiCall(url, method) {
    let xhr = new XMLHttpRequest();
    xhr.withCredentials = true;
    xhr.addEventListener('readystatechange', function () {
        if (this.readyState === this.DONE) {
            const payload = JSON.parse(this.responseText);
            renderItems(payload['pending_items'], 'edit', 'pending_items', editItem);
            renderItems(payload['done_items'], 'delete', 'done_items', deleteItem);
            renderHeader(payload['done_item_count'], payload['pending_item_count']);
        }
    });
    xhr.open(method, url);
    xhr.setRequestHeader('content-type', 'application/json');
    xhr.setRequestHeader('user-token', 'token');
    return xhr;
}

function editItem() {
    const title = this.id.replaceAll('-', ' ').replace('edit ', '');
    const json = {
        'title': title,
        'status': 'DONE',
    };
    apiCall('/v1/item/edit', 'POST').send(JSON.stringify(json));
}

function deleteItem() {
    const title = this.id.replaceAll('-', ' ').replace('delete ', '');
    const json = {
        'title': title,
        'status': 'DONE',
    };
    apiCall('/v1/item/delete', 'DELETE').send(JSON.stringify(json));
}

function getItems() {
    apiCall('/v1/item/get', 'GET').send();
}
getItems();

document.getElementById('create-button').addEventListener('click', createItem);
function createItem() {
    const title = document.getElementById('name');
    apiCall(`/v1/item/create/${title.value}`, 'POST').send();
    document.getElementById('name').value = null;
}
