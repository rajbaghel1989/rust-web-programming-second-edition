use crate::schema::to_do;
use crate::to_do::enums::TaskStatus;
use chrono::{NaiveDateTime, Utc};

#[derive(Insertable)]
#[diesel(table_name = to_do)]
pub struct NewItem {
    pub title: String,
    pub status: String,
    pub date: NaiveDateTime,
}
impl NewItem {
    pub fn new(title: String) -> Self {
        let now = Utc::now().naive_local();
        NewItem {
            title,
            status: TaskStatus::Pending.stringify(),
            date: now,
        }
    }
}
