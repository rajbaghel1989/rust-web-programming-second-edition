use crate::database::establish_connection;
use crate::json_serialization::to_do_items::ToDoItems;
use crate::models::items::item::Item;
use crate::models::items::new_item::NewItem;
use crate::schema::to_do;
use actix_web::{HttpRequest, HttpResponse};
use diesel::prelude::*;

pub async fn create(req: HttpRequest) -> HttpResponse {
    let title = req.match_info().get("title").unwrap().to_string();
    let mut connection = establish_connection();
    let items = to_do::table
        .filter(to_do::columns::title.eq(&title.as_str()))
        .order(to_do::columns::id.asc())
        .load::<Item>(&mut connection)
        .unwrap();
    if items.is_empty() {
        let new_item = NewItem::new(title);
        diesel::insert_into(to_do::table)
            .values(&new_item)
            .execute(&mut connection)
            .ok();
    }
    HttpResponse::Ok().json(ToDoItems::get_state())
}
