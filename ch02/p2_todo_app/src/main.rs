use crate::processes::process_input;
use crate::state::read_file;
use crate::to_do::enums::TaskStatus;
use crate::to_do::to_do_factory;
use serde_json::{Map, Value};
use std::env;

mod processes;
mod state;
mod to_do;

fn main() {
    let file_name = "./state.json";
    let args: Vec<String> = env::args().collect();
    if args.len() < 3 {
        panic!("Missing required arguments");
    }
    let command: &String = &args[1];
    let title: &String = &args[2];
    let state: Map<String, Value> = read_file(file_name);
    let status = match &state.get(*&title) {
        Some(result) => result.to_string().replace('\"', ""),
        None => "pending".to_owned(),
    };
    let item = to_do_factory(title, TaskStatus::from_string(status));
    process_input(item, command.to_string(), &state);
}
