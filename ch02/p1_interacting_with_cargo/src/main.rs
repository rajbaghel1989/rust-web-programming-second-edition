use clap::{Arg, Command};

fn main() {
    let first_name = Arg::new("first name")
        .short('f')
        .long("first_name")
        .help("first name of user")
        .required(true);
    let last_name = Arg::new("last name")
        .short('l')
        .long("last_name")
        .help("last name of user")
        .required(true);
    let age = Arg::new("age")
        .short('a')
        .long("age")
        .help("age of user")
        .value_parser(clap::value_parser!(i8))
        .required(true);
    let app = Command::new("booking")
        .version("1.0")
        .about("Books in a user")
        .author("Ed")
        .arg(first_name)
        .arg(last_name)
        .arg(age);
    let matches = app.get_matches();
    let name = matches.get_one::<String>("first name")
        .expect("First name is required");
    let surname = matches.get_one::<String>("last name")
        .expect("Last name is required");
    let age = *matches.get_one::<i8>("age")
        .expect("Age is required");

    println!("name = {:?}", name);
    println!("surname = {:?}", surname);
    println!("age = {:?}", age);
}
